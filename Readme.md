# CarRacing-v0
This is a pytorch implementation to solve the OpenAi car-racing environment, https://gym.openai.com/envs/CarRacing-v0/.

In this script we will train an agent through the PPO algorithm.
## Installation
To install requirements, first run `./setup.sh` script to set up the environment. 

Next activate the environment through `source .env/bin/activate `

## Training usage ##

To perform training of the car-racing environment you can run the script:
```
python train.py
````

If you wish to visualize the environment while training, please pass the `--render` argument:
```
python train.py --render
```

Optionally you can specify through `-o` the output directory where results and weights of your model will be saved, otherwise they will be saved in a folder called `results`. E.g.
```
python train.py --render -o output_folder
```

### Training Hyperparameters

Most of the hyperparameters are configurable in a `config.json` file, found inside the `config` folder. Here you can tune the parameters in order to find the most adapt, as well as setting a patience for the early stop functionality.

Inside of the `config` folder is present also a `version.py` file indicating the version of the current script.


### Output

In the output folder will be saved:
- The weights of the best model trained according to the best scoring average in a span of 10 episodes, `car_racing_best_weights.pkl`
- A text file called `results.txt` logging every 10 episodes the score of the current episode and the average score of the last 10 episodes.

IMPORTANT: Every time a new training script starts, the content of the output folder will be removed if the `-o` argument shares the same name of the current output folder. If you wish not to remove the weights of the best model and the logging results, you should provide a different name for the output folder, e.g.:
``` python train.py --render -o different_output_folder```

## Testing usage ##

As for the training example, you can perform the testing of a model with the command:
```
python test.py --render
```

If the output folder was not the default one, you can specify where the tester should find the path of the model through the `-m` argument:
```
python test.py --render -m output_folder
```


