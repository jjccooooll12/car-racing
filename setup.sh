#!/bin/bash
set -euo pipefail

sudo apt-get install python3-venv

python3 -m venv .env

source .env/bin/activate

pip install -r requirements.txt

git clone https://github.com/openai/gym
cd gym
pip install -e .

