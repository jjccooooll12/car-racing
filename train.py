import sys
sys.path.append("./src")
sys.path.append("./config")

import argparse
import json
import numpy as np
import os

from version import CAR_RACING_VERSION
from car_environment import CarWrapper
from ppo_learner_agent import Agent
from utils import CheckConfig, is_file

class CarTrainer():
    """
    Trainer of the Car Agent 
    """ 
    
    def __init__(self, args):
        
        self.args = args
        
        #check if the config file is present
        self.config = is_file("./config/config.json")
        
        #check if fields in the configuration file are correct
        checkConfig = CheckConfig(self.config)
        
        with open(self.config) as json_file:
            self.config = json.load(json_file)

        self.config["outputdir"] = self.args.outputdir

        self.env = CarWrapper(self.config)
        self.agent = Agent(self.config)
        
        
    
    def save_best_model(self, average_score, best_score, early_stop):
        if average_score > best_score:
            self.agent.save_weights()
            best_score = average_score
            early_stop = self.config["patience"]
        else:
            early_stop -= 1    

        return best_score, early_stop
    
    
    
    def log_results(self, episode, score, average_score, log_interval=10):
        if episode == 0:
            if os.path.isfile(self.args.outputdir+"/results.txt"):
                os.remove(self.args.outputdir+"/results.txt")
            with open(self.args.outputdir+"/results.txt", "a") as f:
                f.write("Configuration parameters:" +'\n'+'\n')
                for k,v in self.config.items():
                    f.write(k+' : '+str(v) +'\n') 
                f.write('\n')        
        if episode % log_interval == 0:
            print(f'Episode {episode}  Last score: {score:.2f}  Average score of last 10 episodes: {average_score:.2f}')
            with open(self.args.outputdir+"/results.txt", "a") as f:
                f.write(f'Episode: {episode}, Last score: {score:.2f}, Average score: {average_score:.2f}'+'\n')

             
            
    @staticmethod
    def normalize_steering(action):
        """
        The car tends to go to the right with the current setup so we try to correct this 
        by incrementing the steering value
        """
                                       # steer, gas, break        
        steering_normalizer_mu = np.array([2.,   1.,   1.]) 
        steering_normalizer_add = np.array([-1,  0.,   0.])

        return action * steering_normalizer_mu + steering_normalizer_add
    
    
    
    def update_ppo(self, transition):
        if self.agent.is_buffer_full(transition):
            print('Updating PPO...')
            self.agent.learn_ppo()

        
        
    def run_agent(self):
        print("\n" + "CarRacing " + CAR_RACING_VERSION  +"\n")

        self.env.reset()
        early_stop = self.config["patience"]
        best_score = -100
        score_history = []

        # run the agent
        for episode in range(self.config["n_episodes"]):
            steps = 1
            score = 0
            done = False
            state = self.env.reset()

            while not done:
                
                # get new state and probabilities
                action, probs = self.agent.select_action(state)
                new_state, reward, done, no_progress = self.env.step(self.normalize_steering(action))
              
                if self.args.render:
                    self.env.render()
                
                # update the ppo alogorithm
                transition = (state, action, probs, reward, new_state)
                self.update_ppo(transition)

                score+=reward
                state = new_state

                if no_progress:
                    break

                steps +=1
            
            # save the best model
            score_history.append(score)
            average_score = np.mean(score_history[-10:])
            best_score, early_stop = self.save_best_model(average_score, best_score, early_stop)

            if early_stop == 0:
                print ("Patience has been reached. Stopping the training...")
                break
            
            # log the results
            self.log_results(episode, score, average_score)

        print("Training completed!")
        
        
        
def parse_options():
    parser = argparse.ArgumentParser(description='Train a PPO agent for the CarRacing-v0')

    parser.add_argument('--render', action='store_true', help='render the environment')

    parser.add_argument(
        "--config", help="Config file that contains hyperparameters for the training", default="./config/config.json")

    parser.add_argument(
        "-o", "--outputdir", help="Name of the output folder where the model will be saved", default="results")

    args = parser.parse_args()

    return args 



if __name__ == "__main__":
    args = parse_options()
    car_trainer = CarTrainer(args)
    car_trainer.run_agent()
    
