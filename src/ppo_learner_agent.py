import torch
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Beta
from torch.utils.data.sampler import BatchSampler, SubsetRandomSampler

import numpy as np
import os

from model import ActorCriticNet

SEED = 42
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
torch.manual_seed(SEED)


class Agent():
    """
    Agent for PPO training
    
    """
    
    def __init__(self, config):
        
        self.config = config
        self.horizon = self.config["horizon"] # number of steps before updating
        
        self.transition = np.dtype([('state', np.float64, (self.config["n_frames"], 96, 96)), ('action', np.float64, (3,)), ('probs', np.float64),('reward', np.float64), ('new_state', np.float64, (self.config["n_frames"], 96, 96))])
        
        self.model = ActorCriticNet(config).double().to(device)
        self.buffer = np.empty(self.horizon, dtype=self.transition)
        self.counter = 0
        self.optimizer = optim.Adam(self.model.parameters(), lr=self.config["learning_rate"])

        
    def select_action(self, state):
        state = torch.from_numpy(state).double().to(device).unsqueeze(0)
        
        with torch.no_grad():
            actor_a, actor_b = self.model(state)[0]
        distribution = Beta(actor_a, actor_b)
        
        action = distribution.sample()
        probs = distribution.log_prob(action).sum(dim=1)

        action = action.squeeze().cpu().numpy()
        probs = probs.item()
        return action, probs
    

    def save_weights(self):
        if not os.path.exists(self.config["outputdir"]):
            os.mkdir(self.config["outputdir"]) 
        else:
            for filename in os.listdir('./'+self.config["outputdir"]):
                if filename.startswith('car_racing'):
                    os.remove(self.config["outputdir"]+'/'+filename) 
        torch.save(self.model.state_dict(), self.config["outputdir"]+'/car_racing_best_weights.pkl')
        
        
    def is_buffer_full(self, transition):
        self.buffer[self.counter] = transition
        self.counter += 1
        if self.counter == self.horizon:
            self.counter = 0
            return True
        else:
            return False

     
    def learn_ppo(self):
        policy_clip = self.config["policy_clip"]
        discount_factor = self.config["discount_factor"] #gamma
        
        state = torch.tensor(self.buffer['state'], dtype=torch.double).to(device)
        action = torch.tensor(self.buffer['action'], dtype=torch.double).to(device)
        new_state = torch.tensor(self.buffer['new_state'], dtype=torch.double).to(device)
        reward = torch.tensor(self.buffer['reward'], dtype=torch.double).to(device).view(-1, 1)
        old_probs = torch.tensor(self.buffer['probs'], dtype=torch.double).to(device).view(-1, 1)
        
        critic_value_new_state = self.model(new_state)[1]
        critic_value_state = self.model(state)[1]
        value_loss_coefficient = 2.

        with torch.no_grad():
            value_returned = reward + discount_factor * critic_value_new_state
            advantage = value_returned - critic_value_state

        sampler = SubsetRandomSampler(range(self.horizon))
        for epoch in range(self.config["n_epochs"]):
            for batch in BatchSampler(sampler=sampler, batch_size=self.config["batch_size"], drop_last=False):
                actor_a, actor_b = self.model(state[batch])[0]
                #using a distribution, exploration takes place
                distribution = Beta(actor_a, actor_b)
                probs = distribution.log_prob(action[batch]).sum(dim=1, keepdim=True)
                
                #main difference of the ppo algorithm lies on the ratio (new_log_probs/old_log_probs).exp() for the loss
                probs_ratio = torch.exp(probs - old_probs[batch])
                
                #surrogate functions
                weighted_probs = probs_ratio * advantage[batch] 
                weighted_clipped_probs = torch.clamp(probs_ratio, 1.0 - policy_clip, 1.0 + policy_clip) * advantage[batch]
                
                #we take the minimum of the 2 surrogate functions
                actor_loss = -torch.min(weighted_probs, weighted_clipped_probs).mean()
                
                critic_value_batch_state = self.model(state[batch])[1]
                #smoothing to reduce variance
                value_loss = F.smooth_l1_loss(critic_value_batch_state, value_returned[batch], beta=1.0)
                #total loss is the sum of clipped actor loss and critic value loss
                loss = actor_loss + value_loss_coefficient * value_loss

                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
