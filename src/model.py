import torch
import torch.nn as nn

# CREDITS: architecture for the task from https://github.com/zddkjmuner/OpenAI-Racing-Car-using-PPO/blob/master/test.py
class ActorCriticNet(nn.Module):
    """
    Actor-Critic Net for PPO
    """

    def __init__(self, config):
        super(ActorCriticNet, self).__init__()
        
        self.config = config
        num_inputs = self.config["n_frames"]
        num_outputs = self.config["num_actions"]
        
        self.cnn_base = nn.Sequential(  # input shape (4, 96, 96)
            nn.Conv2d(num_inputs, 8, kernel_size=4, stride=2),
            nn.ReLU(),  # activation
            nn.Conv2d(8, 16, kernel_size=3, stride=2),  # (8, 47, 47)
            nn.ReLU(),  # activation
            nn.Conv2d(16, 32, kernel_size=3, stride=2),  # (16, 23, 23)
            nn.ReLU(),  # activation
            nn.Conv2d(32, 64, kernel_size=3, stride=2),  # (32, 11, 11)
            nn.ReLU(),  # activation
            nn.Conv2d(64, 128, kernel_size=3, stride=1),  # (64, 5, 5)
            nn.ReLU(),  # activation
            nn.Conv2d(128, 256, kernel_size=3, stride=1),  # (128, 3, 3)
            nn.ReLU(),  # activation
        )  # output shape (256, 1, 1)
        
        #critic evaluates the state
        self.critic = nn.Sequential(nn.Linear(256, 100), nn.ReLU(), nn.Linear(100, 1))
        self.fc = nn.Sequential(nn.Linear(256, 100), nn.ReLU())
        self.actor_head_a = nn.Sequential(nn.Linear(100, num_outputs), nn.Softplus())
        self.actor_head_b = nn.Sequential(nn.Linear(100, num_outputs), nn.Softplus())
        self.apply(self._weights_init)

    @staticmethod
    def _weights_init(m):
        if isinstance(m, nn.Conv2d):
            nn.init.xavier_uniform_(m.weight, gain=nn.init.calculate_gain('relu'))
            nn.init.constant_(m.bias, 0.1)

    def forward(self, x):
        x = self.cnn_base(x)
        x = x.view(-1, 256)
        critic_value = self.critic(x)
        x = self.fc(x)
        actor_a = self.actor_head_a(x) + 1
        actor_b = self.actor_head_b(x) + 1

        return (actor_a, actor_b), critic_value
    