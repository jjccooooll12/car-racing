import torch
from model import ActorCriticNet

SEED = 42
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
torch.manual_seed(SEED)


class Agent():
    """
    Agent for testing
    """

    def __init__(self, config):
        self.model = ActorCriticNet(config).float().to(device)
        self.config = config

    def select_action(self, state):
        state = torch.from_numpy(state).float().to(device).unsqueeze(0)
        
        with torch.no_grad():
            actor_a, actor_b = self.model(state)[0]
        action = actor_a / (actor_a + actor_b)

        action = action.squeeze().cpu().numpy()
        return action

    def load_param(self):
        self.model.load_state_dict(torch.load(self.config["model_path"]+'/car_racing_best_weights.pkl'))