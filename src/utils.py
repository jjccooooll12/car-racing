import os
import typing as T
import io
import json

#check if a file is present in the directory
def is_file(file):
    if os.path.isfile(file):
        return file
    else:
        raise FileNotFoundError(f"{file} is not present in the directory, please provide it")
  

 
class CheckConfig(object):
    """
    JSON configuration object for classification. Enforces presence of required fields.
    """

    required_fields = ["discount_factor",
                        "repeated_actions",
                        "n_frames", 
                        "num_actions",  
                        "policy_clip",
                        "n_epochs",
                        "horizon",
                        "batch_size",
                        "learning_rate",
                        "n_episodes",
                        "n_last_steps_for_no_progress",
                        "patience"]

    __slots__ = required_fields 

    def __repr__(self):
        return "CheckConfig({})".format(
            [
                "%s=%s" % (f, getattr(self, f, None))
                for f in chain(self.required_fields, self._optional_fields)
            ]
        )

    def __init__(self, json_file: T.Union[str, io.BytesIO]):
        # Load json dict
        if isinstance(json_file, str):
            with open(json_file) as f:
                settings = json.load(f)
        else:
            settings = json.loads(json_file.getvalue())
        assert isinstance(settings, dict)

        # Check field correctness
        invalid_fields = [
            field
            for field in settings
            if field not in self.required_fields and field not in self._optional_fields
        ]
        if invalid_fields:
            raise ValueError(
                "Invalid fields encountered in configuration: %s"
                % " ".join(invalid_fields)
            )
        for key, val in settings.items():
            setattr(self, key, val)
        if any(not hasattr(self, field) for field in self.required_fields):
            raise ValueError(
                "One of required fields not specified. Required fields: %s"
                % ", ".join(self.required_fields)
            )

    def __getitem__(self, item):
        return getattr(self, item)
    