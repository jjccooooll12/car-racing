import gym
import numpy as np

class CarWrapper():
    """
    Environment wrapper for CarRacing 
    """

    def __init__(self, config):
        self.config = config
        self.env = gym.make('CarRacing-v0')
        self.env.seed(42)

    
    def reset(self):
        """
        This reset function wraps the original one in order to:
            - refresh counter and reward_history for the no_progress functionality
            - convert the environment state in grey. This increases the efficiency of the PPO alogrithm.
            - return a stack of frames instead of only one. This also increases the efficiency of the PPO alogrithm.  
        """
        
        self.counter = 0
        self.reward_history = []
        
        state = self.env.reset()
        
        state2gray = self.rgb2gray(state)
        self.stack = [state2gray] * self.config["n_frames"]  
        
        return np.array(self.stack)
    
    
    
    def step(self, action): 
        """
    This step function wraps the original one which returned:
    
   - state (object): an environment-specific object representing the observation of the environment. 
   - reward (float): amount of reward achieved by the previous action. 
    -done (boolean): Done being True indicates the episode has terminated and all tiles have been visited. Environment resets. 
    -info (dict): diagnostic information useful for debugging. 
    
     We wrap the function in order to:
         - penalize more the car when it goes over the grass 
         - reverse the reward score for the done variable
         
        The last step is needed because in the original step function done is penalized by 100. 
        We need to reverse that otherwise the model won't learn correctly.

        Original code:
        if abs(x) > PLAYFIELD or abs(y) > PLAYFIELD:
            done = True
            step_reward = -100
     
        """
        
        total_reward = 0
        no_progress = False
        
        n_last_steps = self.config["n_last_steps_for_no_progress"]
        no_progress_threshold = -0.1
        
        for i in range(self.config["repeated_actions"]):
            
            state, reward, done, _ = self.env.step(action)
            
            # reverse the reward of the done variable
            if done:
                reward += 100
                
            # green penalty
            if np.mean(state[:, :, 1]) > 185.0:
                reward -= 0.05
                
            total_reward+= reward
            self.reward_history.append(reward)
            self.counter+=1
            
            # compute last_progress variable
            if self.counter >= n_last_steps:
                average_last_steps = np.mean(self.reward_history[-n_last_steps:])
                no_progress = True if average_last_steps <= no_progress_threshold else False

            if done or no_progress:
                break
                                     
        state2gray = self.rgb2gray(state)
        self.update_stack(state2gray)
        
        # done -> all tiles have been visited, episode ends
        # no_progress -> no reward in the last N steps
        return np.array(self.stack), total_reward, done, no_progress
    
    
    def render(self):
        self.env.render()

        
    # see https://stackoverflow.com/questions/41971663/use-numpy-to-convert-rgb-pixel-array-into-grayscale
    @staticmethod
    def rgb2gray(rgb):
        gray = np.dot(rgb[..., :3], [0.299, 0.587, 0.114])
        # normalize
        gray = gray / 128. - 1.
        return gray    
    
    
    def update_stack(self, state):
        self.stack.pop(0)
        self.stack.append(state)
