# Training Report
The Agent was trained for about 4 hours and scored the best model at epoch 4730 with an average score of the reward of 952.

The training then early stopped at epoch 6220 as the model stopped to learn.

![Plot of the learning curve over the episodes](plot_car_drive.png)

Testing the model over 10 epochs it gave the following results:

Episode 1   Score: 884.87 

Episode: 2  Score: 961.40 

Episode: 3  Score: 852.56 

Episode: 4  Score: 937.06 

Episode: 5  Score: 909.09 

Episode: 6  Score: 858.97 

Episode: 7  Score: 837.33 

Episode: 8  Score: 937.06 

Episode: 9  Score: 894.20 

Episode: 10  Score: 931.03 

Average of 900.04


