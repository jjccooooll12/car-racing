import sys
sys.path.append("./src")
sys.path.append("./config")
import argparse
import numpy as np
import json

from car_environment import CarWrapper
from tester_agent import Agent
from utils import CheckConfig, is_file


class CarTester():
    """
    Testing script of the Car model we trained.
    It will run over 10 episodes and return the score
    """
    
    def __init__(self, args):
        
        self.args = args
        
        #check if the config file is present
        self.config = is_file("./config/config.json")
        
        #check if fields in the configuration file are correct
        checkConfig = CheckConfig(self.config)
        
        with open(self.config) as json_file:
            self.config = json.load(json_file)
       
        self.env = CarWrapper(self.config)
        self.agent = Agent(self.config)
        
        self.config["model_path"] = self.args.model_path
        self.agent.load_param()
            
            
    @staticmethod
    def normalize_steering(action):
        """
        The car tends to go to the right with the current setup so we try to correct this 
        by incrementing the steering value
        """
                                       # steer, gas, break        
        steering_normalizer_mu = np.array([2.,   1.,   1.]) 
        steering_normalizer_add = np.array([-1,  0.,   0.])

        return action * steering_normalizer_mu + steering_normalizer_add
        
        
    def run_agent(self):
        state = self.env.reset()
            
        for episode in range(1,11):
            score = 0
            state = self.env.reset()
            done = False
            
            while not done:
                action = self.agent.select_action(state)
                new_state, reward, done, no_progress = self.env.step(self.normalize_steering(action))
                if self.args.render:
                    self.env.render()
                score += reward
                state = new_state
                if no_progress:
                    break
     
            print(f'Episode: {episode}  Score: {score:.2f} ')
     
    
        
def parse_options():
    parser = argparse.ArgumentParser(description='Test a PPO agent for the CarRacing-v0')

    parser.add_argument('--render', action='store_true', help='render the environment')

    parser.add_argument(
        "--config", help="Config file that contains hyperparameters for the training", default="./config/config.json")

    parser.add_argument(
        "-m", "--model_path", help="Name of the path folder where the model is saved", default="results")

    args = parser.parse_args()

    return args 



if __name__ == "__main__":
    args = parse_options()
    car_tester = CarTester(args)
    car_tester.run_agent()
   